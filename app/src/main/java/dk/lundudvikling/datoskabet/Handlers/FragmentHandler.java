package dk.lundudvikling.datoskabet.Handlers;

import android.support.v4.app.FragmentManager;

import dk.lundudvikling.datoskabet.Fragments.BaseFragment;
import dk.lundudvikling.datoskabet.MainActivity;
import dk.lundudvikling.datoskabet.R;

public class FragmentHandler {
    private FragmentManager fm;

    public FragmentHandler(MainActivity activity){
        fm = activity.getSupportFragmentManager();
    }
    public FragmentManager getFragmentManager(){
        return fm;
    }

    public void startTransactionNoBackStack(BaseFragment fragment) {
        fm.beginTransaction().replace(R.id.mainView, fragment).commitAllowingStateLoss();
    }
    public void startTransactionWithBackStack(BaseFragment fragment){
        fm.beginTransaction().replace(R.id.mainView, fragment).addToBackStack(null).commit();
    }
}
