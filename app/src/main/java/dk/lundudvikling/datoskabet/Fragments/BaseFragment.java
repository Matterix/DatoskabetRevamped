package dk.lundudvikling.datoskabet.Fragments;

import android.support.v4.app.Fragment;

import dk.lundudvikling.datoskabet.MainActivity;

public class BaseFragment extends Fragment {
    private MainActivity mainActivity;

    public MainActivity getMainActivity() {
        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();
        return mainActivity;
    }

    public BaseFragment(){
        super();
    }
}
