package dk.lundudvikling.datoskabet.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.datoskabet.Handlers.FragmentHandler;
import dk.lundudvikling.datoskabet.R;

public class ItemsFragment extends BaseFragment {

    private FragmentHandler fragmentHandler;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_items, container, false);
        ButterKnife.bind(this, rootView);
        fragmentHandler = new FragmentHandler(getMainActivity());
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.btn_cabinet) void onBtnCabinetClicked(){
        fragmentHandler.startTransactionWithBackStack(new CabinetFragment());
        Toast.makeText(getContext(), "Hej", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.btn_freezer) void onBtnFreezerClicked(){
        fragmentHandler.startTransactionWithBackStack(new FreezerFragment());
        Toast.makeText(getContext(), "Hej", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_fridge) void onBtnFridgeClicked(){
        fragmentHandler.startTransactionWithBackStack(new FridgeFragment());
        Toast.makeText(getContext(), "Hej", Toast.LENGTH_SHORT).show();
    }

}
